### SonarQube server alongiwith MySQL ###

This is a dockerized sonarqube application with a MySQL database. 
Application and containers stoodup via the docker-compose.yml file.  

Future enhancements :
- Build a SonarQube image with plugins installed in them.

Clone the repository and run the following command

## docker-compose up  

This would spawn 2 containers, one for sonarQube and another for mySQL. 
mySQL exposes the default 3306 port. 
sonarQube server exposes the default 9000 port. 

Access the SonarQube dashboard using http://localhost:9000/